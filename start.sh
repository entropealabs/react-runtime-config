#!/bin/bash
set -euo pipefail

IFS=$'\t\n'

create_json () {
	local static=$(echo $1 | sed "s#/app/$2-src#/usr/share/nginx/html/$2#g")
	local extension=$(echo $static | sed "s#\.jsonnet#\.json#g")
	local xbase=${extension##*/}
	local xpref=${xbase%.*}
	local output=$(echo $extension | sed "s#$xpref/##")
	echo $output
	echo $output | sed -E 's/[^\/]+\.json//g' | xargs mkdir -p
	jsonnet -V env=$ENV $1 | jq -c . > $output
}

build () {
	find "/app/$1-src" -type f -name '*.jsonnet' -print0 |
		while read -d '' file; do
			create_json $file $1
		done
}

build "config"
