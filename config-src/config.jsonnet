local api_url = {
  dev: "http://localhost:4000",
  uat: "https://uat.myapi.com",
  staging: "https://staging.myapi.com",
  production: "https://prod.myapi.com"
};


{
  "api": api_url[std.extVar('env')]
}
