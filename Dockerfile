# Build step
FROM node:11.15 as build
WORKDIR /app
COPY ./package.json .
COPY ./package-lock.json .
RUN npm install
COPY public /app/public/
COPY src /app/src/
RUN npm run-script build --frozen-lockfile --non-interactive

# Release step
FROM nginx:1.17.3-alpine
RUN apk add --no-cache \
      jsonnet --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community \
      bash \
      jq --repository=http://dl-cdn.alpinelinux.org/alpine/edge/main

COPY ./config-src /app/config-src
COPY ./start.sh /app/start.sh
RUN chmod +x /app/start.sh
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf
COPY --from=build /app/build /usr/share/nginx/html

CMD /app/start.sh && exec nginx -g 'daemon off;'
