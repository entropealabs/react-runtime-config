import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import doCreateStore from './store';
import App from './App';
import * as serviceWorker from './serviceWorker';
import './index.css';

const load_config = () =>
  fetch("/config.json")
    .then(data => data.json())


const do_render = (store) =>
  ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>, 
    document.getElementById('root')
  );

load_config()
  .then(json => {
    do_render(doCreateStore(json.api))
  })

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
