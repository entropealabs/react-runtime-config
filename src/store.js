import { createStore } from 'redux';

const initialState = {
  api: ""
}

function updateState(state, action){
	let new_state = {...state}
  switch(action.type){
    default:
			break
  }
	return new_state
}

function doCreateStore(api){
  initialState.api = api
  return createStore(
    updateState, 
    initialState,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
}

export default doCreateStore
