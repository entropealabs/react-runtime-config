import React from 'react';
import Test from './Test';
import { Switch, Route, Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';

export const history = createBrowserHistory()

export const uuidv4 = () =>
  ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
    (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
  )

const App = () => (
 <Router history={history}>
  <Switch>
    <Route exact path="/" component={Test} />
  </Switch>
 </Router>
)

export default App;
