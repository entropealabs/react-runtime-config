import React from 'react';
import { useSelector } from 'react-redux';

export default function Test(){
  const api = useSelector(state => state.api);

  return(
    <div className='container'>
      <div className='card login'>
        <h1 className='center'>React Runtime Configuration</h1>
        <h5>Talking to the API at: {api}</h5>
      </div>
    </div>
  )
}

