# React Runtime Configuration

This example application utilizes Jsonnet and Docker to provide runtime/deploytime configuration for your static React application. In the example, we will configure the API url used to interact with our backend systems. This could be expanded to all sorts of configuration variables needed by your application at runtime, that would change per deployment environment.

### Development locally

There is a config.json file at the root `public` directory. This file is used for non-Docker local development. Your keys here should match those exported in your `config-src/config.jsonnet` file.

### Docker development and deployment

When deploying with Docker, a Jsonnet file is compiled from `config-src` on startup and copied over to the nginx directory for hosting, overriding the default `config.json`.

Currently only one environment variable is given to the Jsonnet compiler, `env`. To add more simply update the `start.sh` file on line 14.

### Testing

There is a `default.env` file in the repo, before starting your docker container, update the exported variables in this file and run `source default.env` or create a new file and `source` that.

You can look at `config-src/config.jsonnet` to see how the env variable is utilized and which values are supported. The current pattern works fine for simple configurations, but you may want to read up on [Jsonnet](https://jsonnet.org/) to explore more complex situations.

To see how the configuration is loaded into the application at runtime checkout `src/index.js`.

### Running
```bash
$ docker build .
$ source default.env
$ docker run -e ENV -p 8080:80 <build hash from first step eg; a6f3ad43cb36>
```

Now point your browser to [http://localhost:8080](http://localhost:8080) to view the API value based on your local ENV variable.

To see how this works in different envs, `ctrl+c` to stop your container,  update `default.env` to a new value that is available in `config-src/config.json` eg; `production`, `source default.env` and run `docker run -e ENV -p 8080:80 <same hash>`

Reload [http://localhost:8080](http://localhost:8080) and you should see the new API value.

### Kubernetes

Should I use this method in k8s? 

I think it works perfectly well in k8s, but there is another method. You can utilize ConfigMaps and whatever variable injection capabilities you have eg; Helm and ArgoCD to achieve the same thing by mounting the ConfigMap as config.json in your container.

### Fail fast
If the Jsonnet fails to compile at startup for whatever reason, maybe the value you have set for ENV on your host doesn't exist in your Jsonnet file(s) on startup, it will fail with the error from the Jsonnet compilation and stop startup immediately.

That being said, you should definitely test your Jsonnet compilation in your CI/Testing phase.
